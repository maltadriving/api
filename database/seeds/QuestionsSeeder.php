<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 13.11.16
 * Time: 21:14
 */
use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    public function run()
    {
        DB::table('questions')->insert([
            'text' => '1 Before making a U - turn in the road you should always:',
            'courseId' => 1,
            'topicId'  => 1
        ]);
        DB::table('questions')->insert([
            'text' => '2 Before making a U - turn in the road you should always:',
            'courseId' => 1,
            'topicId'  => 1
        ]);
        DB::table('questions')->insert([
            'text' => '3 Before making a U - turn in the road you should always:',
            'courseId' => 1,
            'topicId'  => 1
        ]);
    }

}