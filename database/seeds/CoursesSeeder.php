<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 13.11.16
 * Time: 21:14
 */
use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    public function run()
    {
        DB::table('courses')->insert([
            'title' => 'Cars'
        ]);
    }

}