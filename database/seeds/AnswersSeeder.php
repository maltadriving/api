<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 13.11.16
 * Time: 21:14
 */
use Illuminate\Database\Seeder;

class AnswersSeeder extends Seeder
{
    public function run()
    {
        DB::table('answers')->insert([
                [
                    'text' => 'Select a higher gear than normal',
                    'isCorrect' => false,
                    'questionId' => 1
                ],
                [
                    'text' => 'Signal so that other drivers can slow down',
                    'isCorrect' => false,
                    'questionId' => 1
                ],
                [
                    'text' => 'Look over your shoulder for final confirmation',
                    'isCorrect' => true,
                    'questionId' => 1
                ],
                [
                    'text' => 'Give another signal as well as using your indicators',
                    'isCorrect' => false,
                    'questionId' => 1
                ],
            ]
        );
        DB::table('answers')->insert([
                [
                    'text' => 'Select a higher gear than normal',
                    'isCorrect' => false,
                    'questionId' => 2
                ],
                [
                    'text' => 'Signal so that other drivers can slow down',
                    'isCorrect' => false,
                    'questionId' => 2
                ],
                [
                    'text' => 'Look over your shoulder for final confirmation',
                    'isCorrect' => true,
                    'questionId' => 2
                ],
                [
                    'text' => 'Give another signal as well as using your indicators',
                    'isCorrect' => false,
                    'questionId' => 2
                ],
            ]
        );
        DB::table('answers')->insert([
                [
                    'text' => 'Select a higher gear than normal',
                    'isCorrect' => false,
                    'questionId' => 3
                ],
                [
                    'text' => 'Signal so that other drivers can slow down',
                    'isCorrect' => false,
                    'questionId' => 3
                ],
                [
                    'text' => 'Look over your shoulder for final confirmation',
                    'isCorrect' => true,
                    'questionId' => 3
                ],
                [
                    'text' => 'Give another signal as well as using your indicators',
                    'isCorrect' => false,
                    'questionId' => 3
                ],
            ]
        );
    }

}