<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 13.11.16
 * Time: 21:14
 */
use Illuminate\Database\Seeder;

class TopicsSeeder extends Seeder
{
    public function run()
    {
        DB::table('topics')->insert([
            'title' => 'ALERTNESS'
        ]);
    }

}