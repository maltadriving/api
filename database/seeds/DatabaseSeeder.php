<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $iterator = new DirectoryIterator(__DIR__ . '/../../resources/data');

        /** @var SplFileInfo $dir */
        foreach ($iterator as $dir) {

            if( $dir->isDir() ){
                if( in_array(
                    $dir->getFilename(),
                    ['.', '..', '.git']
                )){
                    continue;
                }

                $course = new \App\Model\Entities\Course();
                $course->title = $dir->getFilename();
                $course->save();

                $questionNumber = 1;
                $courseIterator = new DirectoryIterator($dir->getRealPath());
                /** @var SplFileInfo $file */
                foreach ($courseIterator as $file) {
                    if($file->isDir()){
                        continue;
                    }

                    $topic = new \App\Model\Entities\Topic();
                    $topic->title = str_replace('.md','',$file->getFilename());
                    $topic->save();

                    echo $file->getFilename(), PHP_EOL;

                    $content = file_get_contents(
                        $file->getRealPath()
                    );

                    if( empty($content) ){
                        continue;
                    }

                    $parser = new \App\Model\Markdown\File($content);
                    $questions = $parser->parse();

                    foreach ($questions as $question) {
                        $questionObj = new \App\Model\Entities\Question();
                        $questionObj->text = $question['text'];
                        $questionObj->sourceId = $question['title']['id'];
                        $questionObj->sourceNumber = $question['title']['number'];
                        $questionObj->orderNumber = $questionNumber++;
                        $questionObj->image = $question['image'];
                        $questionObj->courseId = $course->id;
                        $questionObj->topicId = $topic->id;

                        $questionObj->save();

                        $hasImage = !empty($questionObj->image);

                        foreach ($question['answers'] as $answer){
                            $answerObj = new \App\Model\Entities\Answer();
                            $answerObj->questionId = $questionObj->id;
                            $answerObj->text = $answer['text'];
                            $answerObj->image = $answer['image'];
                            $answerObj->isCorrect = boolval($answer['correct']);
                            $answerObj->save();

                            if( !empty($answerObj->image) ){
                                $hasImage = true;
                            }
                        }

                        if( !$hasImage )
                        {
                            //$questionObj->delete();
                        }
                    }
                }
            }
        }
//        $this->call(CoursesSeeder::class);
//        $this->call(TopicsSeeder::class);
//        $this->call(QuestionsSeeder::class);
//        $this->call(AnswersSeeder::class);
    }
}
