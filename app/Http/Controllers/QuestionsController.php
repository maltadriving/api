<?php

namespace App\Http\Controllers;

use App\Model\Entities;
use App\Model\Rest\Transformers\Question;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QuestionsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = ( $id === 'first' )
            ? Entities\Question::where('courseId',1)->orderBy('orderNumber')->firstOrFail()
            : Entities\Question::where('sourceId', $id)->firstOrFail();

        $fractal = new Manager();

        $fractal->parseIncludes(['answers', 'answers.is_correct','correctCount']);

        $resource = new Item($question, new Question());
        $resource->setMeta((new \App\Model\Rest\Meta\Question($question))->asArray());

        return new Response($fractal->createData($resource)->toArray());
    }
}
