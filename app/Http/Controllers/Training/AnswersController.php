<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 20.11.16
 * Time: 1:14
 */

namespace App\Http\Controllers\Training;

use App\Http\Controllers\Controller;
use App\Model\Entities\Answer;
use App\Model\Rest\Transformers\Scalar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AnswersController
 *
 * @package App\Http\Controllers\Training
 */
class AnswersController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $id = $request->id;

        $answer = Answer::find($id);

        if (is_null($answer)) {
            throw new NotFoundHttpException("Answer not found");
        }

        $fractal = new Manager();

        $resource = new Item($answer->isCorrect, new Scalar());

        return new Response($fractal->createData($resource)->toArray());
    }
}
