<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 12.11.16
 * Time: 23:02
 */

namespace App\Model\Entities;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
//    public $id;
//    public $text;
//    public $courseId;
//    public $topicId;

    public function answers()
    {
        return $this->hasMany(Answer::class, 'questionId');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'courseId');
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topicId');
    }
}
