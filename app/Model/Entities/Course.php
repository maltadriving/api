<?php

namespace App\Model\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 12.11.16
 * Time: 22:59
 */
class Course extends Model
{
//    public $id;
//    public $title;

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
