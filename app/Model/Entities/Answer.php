<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 12.11.16
 * Time: 23:03
 */

namespace App\Model\Entities;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
//    public $id;
//    public $text;
//    public $questionId;
//    public $isCorrect;

    public function question()
    {
        return $this->belongsTo(Question::class, 'questionId');
    }
}
