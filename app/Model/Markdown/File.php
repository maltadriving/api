<?php

namespace App\Model\Markdown;

use App\Model\Markdown\Token\Begin;
use App\Model\Markdown\Token\ContentFactory;
use App\Model\Markdown\Token\End;

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 15.11.16
 * Time: 8:08
 */

/**
 * Class File
 *
 * @package App\Model\Markdown
 */
class File
{
    /** @var array  */
    private $lines = [];
    /** @var LineParser  */
    private $lineParser;

    /**
     * File constructor.
     *
     * @param string $content
     */
    public function __construct($content)
    {
        $this->lines = explode(PHP_EOL, $content);

        $this->lineParser = new LineParser();
    }

    /**
     * @return array
     */
    private function tokens()
    {
        $tokens = [new Begin()];

        foreach ($this->lines as $line) {
            $type = $this->lineParser->parse($line);

            if ($type === LineType::BLANK) {
                continue;
            }

            $tokens[] = ContentFactory::create($type, $line);
        }

        $tokens[] = new End();

        return $tokens;
    }

    /**
     * @return array
     */
    public function parse()
    {
        $machine = new Machine($this->tokens());
        return $machine->process();
    }
}
