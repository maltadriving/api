<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 22:25
 */

namespace App\Model\Markdown;

use App\Model\Markdown\Token\AnswerTokenInterface;
use App\Model\Markdown\Token\ContentTokenInterface;
use App\Model\Markdown\Token\HeadTokenInterface;
use App\Model\Markdown\Token\ImageTokenInterface;
use App\Model\Markdown\Token\TokenInterface;

/**
 * Class Machine
 *
 * @package App\Model\Markdown
 */
class Machine
{
    /** @var array  */
    private $tokens;

    /** @var array  */
    private $stack;
    /** @var array  */
    private $expected;
    /** @var array */
    private $currentQuestion;

    /**
     * @return array
     */
    private function map()
    {
        return [
            LineType::BEGIN  => [LineType::HEAD],
            LineType::HEAD   => [LineType::TEXT],
            LineType::TEXT   => [LineType::TEXT, LineType::ANSWER, LineType::TEXT_IMAGE, LineType::ANSWER_IMAGE],
            LineType::TEXT_IMAGE => [LineType::ANSWER, LineType::ANSWER_IMAGE],
            LineType::ANSWER => [LineType::ANSWER, LineType::HEAD, LineType::END],
            LineType::ANSWER_IMAGE => [LineType::ANSWER_IMAGE, LineType::HEAD, LineType::END],
            LineType::END    => []
        ];
    }

    /**
     * Machine constructor.
     *
     * @param array $tokens
     */
    public function __construct(array $tokens)
    {
        $this->tokens = $tokens;

        $this->expected = [LineType::BEGIN];
        $this->stack = [];
    }

    /**
     * @return array
     * @throws ParseException
     */
    public function process()
    {
        /** @var TokenInterface $token */
        foreach ($this->tokens as $token) {
            $this->processToken($token);
        }

        return $this->stack;
    }

    /**
     * @param TokenInterface $token
     * @throws ParseException
     */
    private function processToken(TokenInterface $token)
    {
        $type = $token->type();

        if (!in_array($type, $this->expected)) {
            throw new ParseException("Unexpected token: " . $type . ', expexted: ' . implode(',', $this->expected));
        }

        switch ($type) {
            case LineType::BEGIN:
                break;
            case LineType::HEAD:
                $this->pushQuestion();
                $this->initQuestion();
                $this->applyHead($token);
                break;
            case LineType::TEXT:
                $this->applyText($token);
                break;
            case LineType::ANSWER:
            case LineType::ANSWER_IMAGE:
                $this->applyAnswer($token);
                break;
            case LineType::END:
                $this->pushQuestion();
                break;
            case LineType::TEXT_IMAGE:
                $this->applyImage($token);
                break;
            default:
                throw new ParseException("Unknown type: " . $type);
                break;
        }

        $this->expected = $this->map()[$type];
    }

    /**
     *
     */
    private function initQuestion()
    {
        $this->currentQuestion = [
            'title'   => [],
            'text'    => '',
            'image'   => '',
            'answers' => [],
        ];
    }

    /**
     *
     */
    private function pushQuestion()
    {
        if (isset($this->currentQuestion)) {
            array_push($this->stack, $this->currentQuestion);
        }
    }

    /**
     * @param HeadTokenInterface $token
     */
    private function applyHead(HeadTokenInterface $token)
    {
        $this->currentQuestion['title'] = [
            'id'     => $token->globalId(),
            'course' => $token->course(),
            'number' => $token->content()
        ];
    }

    /**
     * @param ContentTokenInterface $token
     */
    private function applyText(ContentTokenInterface $token)
    {
        $this->currentQuestion['text'] .= $token->content();
    }

    /**
     * @param AnswerTokenInterface $token
     */
    private function applyAnswer(AnswerTokenInterface $token)
    {
        $answer = [
            'text'    => null,
            'image'   => null,
            'correct' => $token->isCorrect(),
        ];

        if ($token instanceof ContentTokenInterface) {
            $answer['text'] = $token->content();
        }

        if ($token instanceof ImageTokenInterface) {
            $answer['image'] = $token->image();
        }

        $this->currentQuestion['answers'][] = $answer;
    }

    /**
     * @param ImageTokenInterface $image
     */
    private function applyImage(ImageTokenInterface $image)
    {
        $this->currentQuestion['image'] = $image->image();
    }
}
