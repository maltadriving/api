<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 08.12.16
 * Time: 15:20
 */

namespace App\Model\Markdown\Token;

use App\Model\Markdown\LineType;
use App\Model\Markdown\ParseException;

/**
 * Class TextImage
 *
 * @package App\Model\Markdown\Token
 */
class TextImage implements TokenInterface, ImageTokenInterface
{
    /** @var string */
    private $image;

    /**
     * TextImage constructor.
     *
     * @param string $content
     * @throws ParseException
     */
    public function __construct($content)
    {
        if (preg_match('/^\!\[\]\((images\/[^\/].[^\/]+)\)\s*$/si', $content, $match) !== 1) {
            throw new ParseException('Corrupt image token: ' . $content);
        }
        $this->image = $match[1];
    }

    /**
     * @return int
     */
    public function type()
    {
        return LineType::TEXT_IMAGE;
    }

    /**
     * @return string
     */
    public function image()
    {
        return $this->image;
    }
}
