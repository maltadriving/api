<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 08.12.16
 * Time: 19:06
 */

namespace App\Model\Markdown\Token;

use App\Model\Markdown\LineType;
use App\Model\Markdown\ParseException;

/**
 * Class AnswerImage
 *
 * @package App\Model\Markdown\Token
 */
class AnswerImage implements TokenInterface, ImageTokenInterface, AnswerTokenInterface
{
    /** @var bool  */
    private $isCorrect;
    /** @var  string */
    private $image;

    /**
     * @return bool
     */
    public function isCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * AnswerImage constructor.
     *
     * @param string $content
     * @throws ParseException
     */
    public function __construct($content)
    {
        if (preg_match('/^\d\.\s+\!\[(correct)?\]\((images\/[^\/].[^\/]+)\)\s*$/si', $content, $match) !== 1) {
            throw new ParseException('Corrupt answer image token: ' . $content);
        }
        $this->image = $match[2];

        $this->isCorrect = $match[1] === 'correct';
    }

    /**
     * @return string
     */
    public function image()
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function type()
    {
        return LineType::ANSWER_IMAGE;
    }
}
