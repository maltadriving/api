<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:28
 */

namespace App\Model\Markdown\Token;

use App\Model\Markdown\LineType;

class Text implements TokenInterface, ContentTokenInterface
{
    private $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function content()
    {
        return $this->content;
    }

    public function type()
    {
        return LineType::TEXT;
    }
}
