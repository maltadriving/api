<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:29
 */

namespace App\Model\Markdown\Token;

use App\Model\Markdown\LineType;
use App\Model\Markdown\ParseException;

/**
 * Class Answer
 *
 * @package App\Model\Markdown\Token
 */
class Answer implements TokenInterface, ContentTokenInterface, AnswerTokenInterface
{
    /** @var  string */
    private $content;
    /** @var bool  */
    private $isCorrect;

    /**
     * Answer constructor.
     *
     * @param $content
     * @throws ParseException
     */
    public function __construct($content)
    {
        if (preg_match('/^\d\.(.+?)$/si', $content, $match) !== 1) {
            throw new ParseException('Corrupt token');
        }

        if (preg_match('/[\*]{2}(.*?)[\*]{2}/si', $match[1], $subMatch) === 1) {
            $this->isCorrect = true;
            $this->content = $subMatch[1];
            return;
        }

        $this->content = $match[1];
        $this->isCorrect = false;
    }

    /**
     * @return mixed
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function type()
    {
        return LineType::ANSWER;
    }

    /**
     * @return bool
     */
    public function isCorrect()
    {
        return $this->isCorrect;
    }
}
