<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:22
 */

namespace App\Model\Markdown\Token;

use App\Model\Markdown\LineType;

class End implements TokenInterface
{
    public function type()
    {
        return LineType::END;
    }
}
