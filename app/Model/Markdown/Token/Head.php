<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:26
 */

namespace App\Model\Markdown\Token;

use App\Model\Markdown\LineType;
use App\Model\Markdown\ParseException;

/**
 * Class Head
 *
 * @package App\Model\Markdown\Token
 */
class Head implements TokenInterface, HeadTokenInterface
{
    /** @var  string */
    private $content;
    /** @var  string */
    private $course;
    /** @var  string */
    private $globalId;

    /**
     * Head constructor.
     *
     * @param string $content
     * @throws ParseException
     */
    public function __construct($content)
    {
        if (preg_match('/^[#]{3}(\d+).+?\-.+?(\w+)(\d+\.\d+)(\s*\!{0,4})?[#]{0,3}$/si', $content, $match) !== 1) {
            throw new ParseException('Corrupt head token: ' . $content);
        }
        $this->globalId = $match[1];
        $this->course   = $match[2];
        $this->content  = $match[3];
    }

    /**
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function type()
    {
        return LineType::HEAD;
    }

    /**
     * @return string
     */
    public function course()
    {
        return $this->course;
    }

    /**
     * @return string
     */
    public function globalId()
    {
        return $this->globalId;
    }
}
