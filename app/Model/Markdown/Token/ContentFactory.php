<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:13
 */
namespace App\Model\Markdown\Token;

use App\Model\Markdown\LineType;
use App\Model\Markdown\ParseException;

/**
 * Class ContentFactory
 *
 * @package App\Model\Markdown\Token
 */
class ContentFactory
{

    /**
     * @param int $type
     * @param string $content
     * @return TokenInterface
     * @throws ParseException
     */
    public static function create($type, $content)
    {
        switch ($type) {
            case LineType::HEAD:
                return new Head($content);
            case LineType::TEXT:
                return new Text($content);
            case LineType::ANSWER:
                return new Answer($content);
            case LineType::ANSWER_IMAGE:
                return new AnswerImage($content);
            case LineType::TEXT_IMAGE:
                return new TextImage($content);
            default:
                throw new ParseException("Unknown token type");
        }
    }
}
