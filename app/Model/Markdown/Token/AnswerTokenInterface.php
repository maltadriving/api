<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:29
 */

namespace App\Model\Markdown\Token;

interface AnswerTokenInterface
{
    public function isCorrect();
}
