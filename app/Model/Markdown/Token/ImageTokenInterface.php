<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 08.12.16
 * Time: 15:21
 */

namespace App\Model\Markdown\Token;

interface ImageTokenInterface
{
    public function image();
}
