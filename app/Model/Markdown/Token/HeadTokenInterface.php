<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 17.11.16
 * Time: 0:19
 */

namespace App\Model\Markdown\Token;

interface HeadTokenInterface extends ContentTokenInterface
{
    public function course();
    public function globalId();
}
