<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:17
 */

namespace App\Model\Markdown\Token;

interface TokenInterface
{
    public function type();
}
