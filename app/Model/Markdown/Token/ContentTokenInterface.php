<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 16.11.16
 * Time: 23:24
 */

namespace App\Model\Markdown\Token;

interface ContentTokenInterface
{
    public function __construct($content);

    public function content();
}
