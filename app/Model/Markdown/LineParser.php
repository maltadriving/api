<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 15.11.16
 * Time: 8:29
 */

namespace App\Model\Markdown;

/**
 * Class LineParser
 *
 * @package App\Model\Markdown
 */
class LineParser
{

    /**
     * @return array
     */
    private function parsers()
    {
        return [
            LineType::BLANK  => function ($line) {
                return empty($line);
            },
            LineType::ANSWER => function ($line) {
                return preg_match('/^\d\.\s+[^\!]+/si', $line) === 1;
            },
            LineType::HEAD   => function ($line) {
                return preg_match('/^[#]{3}[^#]/si', $line) === 1;
            },
            LineType::TEXT_IMAGE   => function ($line) {
                return preg_match('/^\!\[\]\(images\/[^\/].[^\/]+\)\s*$/si', $line) === 1;
            },
            LineType::ANSWER_IMAGE   => function ($line) {
                return preg_match('/^\d\.\s\!\[(correct)?\]\(images\/[^\/]+\.[^\/]+\)\s*$/si', $line) === 1;
            },
            LineType::TEXT   => function ($line) {
                return !(empty($line) || preg_match('/^([#]{3}|\d\.|.+?\]\()/si', $line) === 1);
            },
        ];
    }

    /**
     * @param $line
     * @return int|string
     * @throws ParseException
     */
    public function parse($line)
    {
        $cleanLine = trim($line);

        foreach ($this->parsers() as $type => $parser) {
            if ($parser($cleanLine) === true) {
                return $type;
            }
        }

        throw new ParseException('Line type is unknown: ' . $line);
    }
}
