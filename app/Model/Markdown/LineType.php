<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 15.11.16
 * Time: 8:23
 */

namespace App\Model\Markdown;

class LineType
{
    const BEGIN  = 0;
    const BLANK  = 1;
    const HEAD   = 2;
    const TEXT   = 3;
    const ANSWER = 4;
    const TEXT_IMAGE = 5;
    const ANSWER_IMAGE = 6;
    const END    = PHP_INT_MAX;
}
