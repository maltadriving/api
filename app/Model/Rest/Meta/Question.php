<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 18.12.16
 * Time: 17:59
 */

namespace App\Model\Rest\Meta;

use App\Model\Entities;

/**
 * Class Question
 *
 * @package App\Model\Rest\Meta
 */
class Question
{
    /** @var Entities\Question  */
    private $prev;
    /** @var Entities\Question  */
    private $next;
    /** @var  int */
    private $count;

    /**
     * Question constructor.
     *
     * @param Entities\Question $question
     */
    public function __construct(Entities\Question $question)
    {
        $this->prev = Entities\Question::where('orderNumber', '<', $question->orderNumber)
            ->where('courseId',1)
            ->orderBy('orderNumber', 'desc')->first();
        if (is_null($this->prev)) {
            $this->prev = Entities\Question::where('courseId',1)->orderBy('orderNumber', 'desc')->first();
        }

        $this->next = Entities\Question::where('orderNumber', '>', $question->orderNumber)
            ->where('courseId',1)
            ->orderBy('orderNumber')->first();
        if (is_null($this->next)) {
            $this->next = Entities\Question::where('courseId',1)->orderBy('orderNumber')->first();
        }

        $this->count = Entities\Question::where('courseId',1)->count();
        $this->currentNumber = $question->orderNumber;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        return [
            'prev' => $this->prev->sourceId,
            'next' => $this->next->sourceId,
            'count' => $this->count,
            'currentNumber' => $this->currentNumber,
        ];
    }
}
