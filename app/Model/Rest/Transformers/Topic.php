<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 19.11.16
 * Time: 22:16
 */

namespace App\Model\Rest\Transformers;

use App\Model\Entities;
use League\Fractal\TransformerAbstract;

class Topic extends TransformerAbstract
{
    public function transform(Entities\Topic $topic)
    {
        return [
            'id' => $topic->id,
            'title' => ucfirst(strtolower($topic->title)),
        ];
    }
}
