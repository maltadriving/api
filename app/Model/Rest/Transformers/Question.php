<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 19.11.16
 * Time: 22:14
 */

namespace App\Model\Rest\Transformers;

use App\Model\Entities;
use League\Fractal\TransformerAbstract;

/**
 * Class Question
 *
 * @package App\Model\Rest\Transformers
 */
class Question extends TransformerAbstract
{
    /** @var array  */
    protected $defaultIncludes = [
        'topic'
    ];

    /** @var array  */
    protected $availableIncludes = [
        'answers', 'topic', 'total', 'correctCount'
    ];

    /**
     * @param Entities\Question $question
     * @return array
     */
    public function transform(Entities\Question $question)
    {
        return [
            'id'   => $question->id,
            'text' => $question->text,
            'sourceNumber' => $question->sourceNumber,
            'sourceId' => $question->sourceId,
            'orderNumber' => $question->orderNumber,
            'image' => $question->image,
        ];
    }

    /**
     * @param Entities\Question $question
     * @return \League\Fractal\Resource\Item
     */
    public function includeTopic(Entities\Question $question)
    {
        return $this->item($question->topic, new Topic());
    }

    /**
     * @param Entities\Question $question
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAnswers(Entities\Question $question)
    {
        return $this->collection($question->answers, new Answer());
    }

    /**
     * @param Entities\Question $question
     * @return \League\Fractal\Resource\Item
     */
    public function includeTotal(Entities\Question $question)
    {
        return $this->item(Entities\Question::get()->count(), new Scalar());
    }

    /**
     * @param Entities\Question $question
     * @return \League\Fractal\Resource\Item
     */
    public function includeCorrectCount(Entities\Question $question)
    {
        $count = 0;
        foreach ($question->answers as $answer) {
            if ($answer->isCorrect) {
                $count++;
            }
        }

        return $this->item($count, new Scalar());
    }
}
