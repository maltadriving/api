<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 19.11.16
 * Time: 23:52
 */

namespace App\Model\Rest\Transformers;

use League\Fractal\TransformerAbstract;

class Scalar extends TransformerAbstract
{
    public function transform($value)
    {
        return ['value' => $value];
    }
}
