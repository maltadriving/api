<?php
/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 19.11.16
 * Time: 22:33
 */

namespace App\Model\Rest\Transformers;

use League\Fractal\TransformerAbstract;

class Answer extends TransformerAbstract
{
    protected $availableIncludes = [
        'is_correct'
    ];

    public function transform(\App\Model\Entities\Answer $answer)
    {
        return [
            'id'    => $answer->id,
            'text'  => $answer->text,
            'image' => $answer->image,
        ];
    }

    public function includeIsCorrect(\App\Model\Entities\Answer $answer)
    {
        return $this->item(boolval($answer->isCorrect), new Scalar());
    }
}
