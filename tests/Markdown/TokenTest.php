<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 08.12.16
 * Time: 16:05
 */
use \App\Model\Markdown;

class TokenTest extends TestCase
{
    public static function lines()
    {
        require_once self::fixturesPath() . 'Markdown/LinesProvider.php';
        return LinesProvider::data();
    }

    public function provideAnswers()
    {
        $data = self::lines();

        return [
            $data['answer'],
            $data['correct_answer'],
        ];
    }

    public function provideImageAnswers()
    {
        $data = self::lines();

        return [
            $data['answer_image'],
            $data['correct_answer_image'],
        ];
    }

    public function testTextToken()
    {
        $content = $data = self::lines()['text'];

        $token = new Markdown\Token\Text($content);

        $this->assertInstanceOf(Markdown\Token\TokenInterface::class, $token);
        $this->assertInstanceOf(Markdown\Token\ContentTokenInterface::class, $token);
    }

    public function testHeadToken()
    {
        $fixture = $data = self::lines()['head'];

        $content = $fixture[1];
        $token = new Markdown\Token\Head($content);

        $this->assertInstanceOf(Markdown\Token\TokenInterface::class, $token);
        $this->assertInstanceOf(Markdown\Token\HeadTokenInterface::class, $token);

        $this->assertEquals($fixture[2],$token->globalId());
        $this->assertEquals($fixture[3],$token->course());
        $this->assertEquals($fixture[4],$token->content());
    }

    /**
     * @dataProvider provideAnswers
     */
    public function testAnswerToken($type, $content, $isCorrect)
    {
        $token = new Markdown\Token\Answer($content);

        $this->assertInstanceOf(Markdown\Token\TokenInterface::class, $token);
        $this->assertInstanceOf(Markdown\Token\AnswerTokenInterface::class, $token);

        $this->assertEquals($isCorrect,$token->isCorrect());
    }

    public function testImageToken()
    {
        $fixture = $data = self::lines()['text_image'];

        $content = $fixture[1];
        $token = new Markdown\Token\TextImage($content);

        $this->assertInstanceOf(Markdown\Token\TokenInterface::class, $token);
        $this->assertInstanceOf(Markdown\Token\ImageTokenInterface::class, $token);

        $this->assertEquals($fixture[2],$token->image());
    }

    /**
     * @dataProvider provideImageAnswers
     */
    public function testAnswerImageToken($type, $content, $image, $isCorrect)
    {
        $token = new Markdown\Token\AnswerImage($content);

        $this->assertInstanceOf(Markdown\Token\TokenInterface::class, $token);
        $this->assertInstanceOf(Markdown\Token\AnswerTokenInterface::class, $token);
        $this->assertInstanceOf(Markdown\Token\ImageTokenInterface::class, $token);

        $this->assertEquals($image,$token->image());
        $this->assertEquals($isCorrect,$token->isCorrect());
    }
}