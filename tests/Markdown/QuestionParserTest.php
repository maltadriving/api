<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 15.11.16
 * Time: 8:04
 */
class QuestionParserTest extends TestCase
{
    public function provideFiles()
    {
        return [
            ['Markdown/Alertness.md', 15],
            ['Markdown/Images.md', 2],
        ];
    }

    /**
     * @param $file
     * @param $count
     *
     * @dataProvider provideFiles
     */
    public function testParse($file, $count)
    {
        $content = file_get_contents(self::fixturesPath() . $file);
        $md = new \App\Model\Markdown\File($content);

        $questions = $md->parse();
        $this->assertInternalType('array', $questions);
        $this->assertContainsOnly('array', $questions);
        $this->assertCount($count, $questions);
    }
}
