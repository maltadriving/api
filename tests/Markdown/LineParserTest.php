<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 15.11.16
 * Time: 8:42
 */
class LineParserTest extends TestCase
{

    public function provideLines()
    {
        require_once __DIR__ . '/../Fixtures/Markdown/LinesProvider.php';

        return LinesProvider::data();
    }

    /**
     * @dataProvider provideLines
     */
    public function testParse($type, $line)
    {
        $parser = new \App\Model\Markdown\LineParser();
        $this->assertEquals($type, $parser->parse($line));
    }
}