<?php

/**
 * Created by PhpStorm.
 * User: ksasim
 * Date: 08.12.16
 * Time: 16:00
 */
class LinesProvider
{
    static public function data() {
        return [
            'blank' => [\App\Model\Markdown\LineType::BLANK,  '  '],
            'answer' => [\App\Model\Markdown\LineType::ANSWER, '1. asdasd', false],
            'correct_answer' => [\App\Model\Markdown\LineType::ANSWER, '2. **asdasd**', true],
            'head' => [\App\Model\Markdown\LineType::HEAD,   '###3344 - TEST5.6', '3344', 'TEST', '5.6'],
            'text' => [\App\Model\Markdown\LineType::TEXT,   'question text'],
            'text_image' => [\App\Model\Markdown\LineType::TEXT_IMAGE,   '![](images/testimage.png)', 'images/testimage.png'],
            'answer_image' => [
                \App\Model\Markdown\LineType::ANSWER_IMAGE,
                '1. ![](images/testimage.png)',
                'images/testimage.png',
                false
            ],
            'correct_answer_image' => [
                \App\Model\Markdown\LineType::ANSWER_IMAGE,
                '2. ![correct](images/testimage.png)',
                'images/testimage.png',
                true
            ],
        ];
    }
}