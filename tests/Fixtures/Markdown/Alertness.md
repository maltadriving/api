###2562 - CARS1.2

Before making a U - turn in the road you should always:

1. Select a higher gear than normal
2. Signal so that other drivers can slow down
3. **Look over your shoulder for final confirmation**
4. Give another signal as well as using your indicators

###4212 - CARS1.3

As a driver what do you understandby the term 'Blind Spot'?

1. An area covered by your left hand mirror
2. An area not covered by your headlights
3. An area covered by your right hand mirror
4. **An area not covered by your mirrors**

###4213 - CARS1.7

What does the abbreviation MSM mean?

1. **Mirror signal manoeuvre**
2. Manoeuvre speed mirror
3. Mirror speed manoeuvre
4. Manoeuvre signal mirror

###4214 - CARS1.9

When following a large vehicle you should stay well back because

1. It helps you to keep out of the wind
2. It helps the large vehicle to stop more easily
3. **It allows the driver to see you in the mirror**
4. It allows you to corner more quickly

###4215 - CARS1.10

In which of these following situations should you avoid overtaking?

1. On a 50 kph road
2. In a one-way street
3. Just after a bend
4. **Approaching a dip in the road**

###4216 - CARS1.12

You should not use a mobile phone whilst driving

1. Because reception is poor when the engine is running
2. Unless you are able to drive one handed
3. **Because it might distract your attention from the road ahead**
4. Until you are satisfied that no other traffic is near

###4217 - CARS1.13

Your vehicle is fitted with a hands-free phone system. Using this equipment whilst driving

1. Could be very good for road safety
2. **Could distract your attention from the road**
3. Is recommended by The Highway Code
4. Is quite safe as long as you slow down

###4218 - CARS1.14

Using a hands-free phone is likely to

1. Reduce your view
2. Increase your concentration
3. Improve your safety
4. **Divert your attention**

###4219 - CARS1.15

Using a mobile phone while you are driving

1. Will affect your vehicle
2. Will reduce your field of vision
3. **Could distract your attention from the road**
4. Is acceptable in a vehicle with power steering

###4220 - CARS1.18

You are driving along a narrow country road. When passing a cyclist you should drive

1. Quickly sounding the horn as you pass
2. Quickly leaving plenty of room
3. **Slowly leaving plenty of room**
4. Slowly sounding the horn as you pass

###4221 - CARS1.19

To answer your mobile phone when driving you should:

1. Slow down and allow others to overtake
2. **Stop in a proper and convenient place**
3. Keep the call time to a minimum
4. Reduce your speed wherever you are

###4222 - CARS1.23

Why should you be parked before using a mobile phone?

1. Because the car electrics will be affected
2. **So that the control of your vehicle is not affected**
3. So that a proper conversation can be held
4. Because the reception is better when stopped

###4223 - CARS1.24

You are driving in the dark and are dazzled by the headlights of an oncoming car. You should

1. **Slow down or stop**
2. Pull down the sun visor
3. Flash your headlights
4. Close your eyes

###4224 - CARS1.25

You are driving at dusk. Your lights should be switched on

1. **So that others can see you**
2. **Even when street lights are lit**
3. Only when street lights are lit
4. Only when others have done so

###4225 - CARS1.26

To overtake safely which one of the following applies?

1. **Check the speed and position of following traffic**
2. Steer round the vehicle sharply
3. Get in close behind before moving out
4. Cut back in sharply when you have passed the vehicle
